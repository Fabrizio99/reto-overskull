addEventListener('DOMContentLoaded', () => {
    readPost()
})

function readPost() {
    const container = document.getElementById('posts');
    const subContainer = document.createDocumentFragment()
    const listPost = JSON.parse(localStorage.getItem('listPost'));
    if (!listPost) return
    listPost.forEach(post => {
        const postStructure = document.getElementById('structure').content.cloneNode(true);
        postStructure.querySelector('h4').textContent = post.title;
        postStructure.querySelector('spam').textContent = post.id;

        postStructure.querySelector('.description').textContent = post.text;
        postStructure.querySelector('img').setAttribute('src', `${post.photo}`);
        subContainer.append(postStructure);
    });
    container.appendChild(subContainer);

}