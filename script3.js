$(document).ready(function () {
    let listData;
    $.get("http://sistema.shalomempresarial.com/android/jsonterminal",
        function (data) {
            listData = data.Map;
            fetchDepartments(listData)

        });

});


function fetchDepartments(list) {
    const departmentGroup = document.createDocumentFragment();
    const filteredList = [... new Set(list.map((de) => de.departamento))].filter((e) => e != '').sort();
    for (let index = 0; index < filteredList.length; index++) {
        const departmentTag = document.createElement('li');
        departmentTag.classList.add('list-group-item');
        const departmentLink = document.createElement('a');
        departmentLink.setAttribute('href', '#')
        departmentLink.textContent = filteredList[index]
        departmentLink.addEventListener('click', () => selectDepartment(list, filteredList[index]));
        departmentTag.append(departmentLink);
        departmentGroup.append(departmentTag);
    }
    const departmentsContainer = document.getElementById('department-container');
    departmentsContainer.append(departmentGroup)
}

function selectDepartment(list, departament) {
    const filteredList = list.filter(s => s.departamento == departament);
    drawData(filteredList);
    console.log(list,filteredList)
}

function drawData(listItems) {
    const listTerminalContainer = document.getElementById('search-container')
    listTerminalContainer.textContent = ''
    const searchContainer = document.createElement('div');
    searchContainer.classList.add('col-12');
    const searchSubcontainer = document.createElement('div');
    searchSubcontainer.classList.add('form-group');
    const inputSearh = document.createElement('input');
    inputSearh.setAttribute('type', 'text');
    inputSearh.setAttribute('placeholder', 'Escriba la zona/lugar');
    inputSearh.classList.add('form-control');
    inputSearh.setAttribute('id', 'inputSearch')
    inputSearh.addEventListener('keyup', function () {
        const filtered = listItems.filter((e) => {
            if (e.zona.toLowerCase().includes(inputSearh.value.toLowerCase()) || e.lugar.toLowerCase().includes(inputSearh.value.toLowerCase())) {
                return e;
            }
        })
        fetchData(filtered)
    })
    searchSubcontainer.append(inputSearh)
    searchContainer.append(searchSubcontainer)
    listTerminalContainer.prepend(searchContainer)
    fetchData(listItems)
}

function fetchData(listItems) {
    const container = document.getElementById('container');
    container.textContent = ''

    const subContainer = document.createDocumentFragment();

    let counter = 1;


    listItems.forEach(item => {
        const itemStructure = document.getElementById('structure').content.cloneNode(true);

        itemStructure.querySelector('.title').textContent = `Terminal #${item.ter_id}`;
        itemStructure.querySelector('.id').textContent = item.ter_id;
        itemStructure.querySelector('.zone').textContent = item.zona;
        itemStructure.querySelector('.department').textContent = item.departamento;
        itemStructure.querySelector('.place').textContent = item.lugar;
        itemStructure.querySelector('.latitude').textContent = item.latitud;
        itemStructure.querySelector('.longitud').textContent = item.longitud;
        itemStructure.querySelector('.address').textContent = item.direccion;
        itemStructure.querySelector('.phone-number').textContent = item.telefono;
        itemStructure.querySelector('.hour').textContent = item.hora_atencion;
        itemStructure.querySelector('.details').textContent = item.detalles;
        itemStructure.querySelector('.d1').setAttribute('data-target', `#collapsed${counter}`);
        itemStructure.querySelector('.d2').setAttribute('id', `collapsed${counter}`);

        const destineList = [...new Set(listItems.filter(e => e.ter_id == item.ter_id)[0].destinos.map(e => e.PROVINCIAS))].sort()
        const destineSelects = itemStructure.querySelector('.destineSelect');
        const destineGroup = document.createDocumentFragment();
        destineList.forEach(element => {
            const destine = document.createElement('option');
            destine.setAttribute('value', element);
            destine.textContent = element;
            destineGroup.append(destine)
        });
        destineSelects.append(destineGroup);

        const destineButton = itemStructure.querySelector('.searchDestine');
        destineButton.addEventListener('click', function () {
            const destinesTerminal = listItems.filter(e => e.ter_id == item.ter_id)[0].destinos.filter(e => e.PROVINCIAS == destineSelects.value)
            listDestines(destinesTerminal)
        })


        subContainer.append(itemStructure);
        container.appendChild(subContainer);
        counter++
    })
};
function listDestines(item) {
    const destineContainer = document.createDocumentFragment();
    let destine = 1;
    item.forEach(de => {
        const destineStructure = document.getElementById('destineStructure').content.cloneNode(true);
        destineStructure.querySelector('.title').textContent = `#${destine}`;
        destineStructure.querySelector('.b1').setAttribute('data-target', `#collapse${item.ter_id}${destine}`);
        destineStructure.querySelector('.b2').setAttribute('id', `collapse${item.ter_id}${destine}`);
        destineStructure.querySelector('.origen').textContent = de.ORIGEN
        destineStructure.querySelector('.out-hour').textContent = de.HORA_DE_SALIDA;
        destineStructure.querySelector('.provincia').textContent = de.PROVINCIAS;
        destineStructure.querySelector('.enter-day').textContent = de.NRO_DE_DIA_DE_LLEGADA;
        destineStructure.querySelector('.in-hour').textContent = de.HORA_DE_LLEGADA;
        destineStructure.querySelector('.sobres').textContent = de.SOBRES;
        destineStructure.querySelector('.min-cost').textContent = de.COSTOMIN;
        destineStructure.querySelector('.weight').textContent = de.PESO;
        destineStructure.querySelector('.volume').textContent = de.VOLUMEN;
        destineStructure.querySelector('.package').textContent = de.PAQUETERIA;
        destine++;
        destineContainer.append(destineStructure);
    });
    const destineC = document.getElementById('destineContainer');
    destineC.textContent = ''
    destineC.append(destineContainer);
}