# Reto - Overskull

Este reto consiste en visualizar la información brindada de la empresa Shalom (JSON) en una página web.

En la página principal se muestra del lado izquierdo un listado de todos los departamentos donde puedan estar ubicados todas las terminales,
donde al hacer click se podrá visualizar todas las terminales de ese departamento.

Ni bien se halla seleccionado un departamento, se tiene un buscador donde se puede escribir la zona o lugar de la terminal que esta buscando, cada vez que se escriba la zona o lugar que busca, la página automáticamente filtrara los resultados.
Como cada terminal posee varios pedidos, hay una sección llamada "Pedidos" que donde al hacer click podra visualizar un pequeño buscador para buscar los destinos por provincia, primero selecciona el destino que desea buscar y luego le da click en buscar.