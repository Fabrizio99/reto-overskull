class Post {
    constructor(title, text) {
        this.photo = 'https://picsum.photos/200/300';
        this.title = title
        this.text = text
        this.id = chance.guid();
    }
}

addEventListener('DOMContentLoaded', () => {
    drawPosts()
})

const addButton = document.getElementById('addButton');
if (addButton) {
    addButton.addEventListener('click', () => {
        createPostDefault();
    })
}


function drawPosts() {
    const listPost = JSON.parse(localStorage.getItem('listPost'));
    const container = document.getElementById('posts');
    const subContainer = document.createDocumentFragment()
    container.textContent = '';
    if (listPost) {
        listPost.forEach(post => {
            const postStructure = document.getElementById('structure').content.cloneNode(true);
            postStructure.querySelector('h4').textContent = post.title;
            postStructure.querySelector('spam').textContent = post.id;

            postStructure.querySelector('.description').textContent = post.text;
            postStructure.querySelector('img').setAttribute('src', `${post.photo}`);
            postStructure.getElementById('delete').addEventListener('click', () => { deletePost(post.id) })

            subContainer.append(postStructure);
        });
        container.appendChild(subContainer);
    }


    const editTitles = document.querySelectorAll('.edit-title');
    editTitles.forEach(element => {
        element.addEventListener('focus', () => {
            element.parentNode.querySelector('.editButton').style.display = 'inline-block'
        })
    });

    const editDescriptions = document.querySelectorAll('.edit-description');
    editDescriptions.forEach(element => {
        element.addEventListener('focus', () => {
            element.parentNode.parentNode.parentNode.querySelector('.editButton').style.display = 'inline-block'
        })
    });

    const editButton = document.querySelectorAll('.editButton')
    editButton.forEach(button => {
        button.addEventListener('click', () => {
            let title = button.parentNode.parentNode.parentNode.querySelector('#titlePost').textContent
            let description = button.parentNode.parentNode.parentNode.querySelector('#descriptionPost').textContent
            let id = button.parentNode.parentNode.parentNode.querySelector('spam').textContent

            const listPost = JSON.parse(localStorage.getItem('listPost'));

            for (let index = 0; index < listPost.length; index++) {
                if (listPost[index].id != id) continue
                listPost[index].title = title;
                listPost[index].text = description;
                break;
            }
            localStorage.setItem('listPost', JSON.stringify(listPost))
            drawPosts()
        })
    });
}

function editPost(id) {
    console.log(id)
}



function deletePost(id) {
    const listPost = JSON.parse(localStorage.getItem('listPost'));
    for (let index = 0; index < listPost.length; index++) {
        if (listPost[index].id != id) continue
        listPost.splice(index, 1)
        break
    }
    localStorage.setItem('listPost', JSON.stringify(listPost));

    drawPosts()
}
function createPostDefault() {
    const listPost = (localStorage.getItem('listPost')) ? JSON.parse(localStorage.getItem('listPost')) : [];
    listPost.unshift(new Post('Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo, culpa similique cupiditate ea mollitia tempore inventore praesentium doloribus hic. Id ad, illo quia omnis voluptate aut adipisci rem velit vitae!'))
    localStorage.setItem('listPost', JSON.stringify(listPost));
    drawPosts()
}

function readPost() {
    const listPost = JSON.parse(localStorage.getItem('listPost'));
    if (!listPost) return
    listPost.forEach(post => {
        const postStructure = document.getElementById('structure').content.cloneNode(true);
        postStructure.querySelector('h4').textContent = post.title;
        postStructure.querySelector('spam').textContent = post.id;

        postStructure.querySelector('.description').textContent = post.text;
        postStructure.querySelector('img').setAttribute('src', `${post.photo}`);
        subContainer.append(postStructure);
    });
    container.appendChild(subContainer);

}